//
//  GameScene.swift
//  ZombieConga
//
//  Created by Parrot on 2019-01-29.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    var timeOfLastUpdate: TimeInterval = 0.0
   
    // Make zombie global, so other functions can access it
    let zombie:SKSpriteNode = SKSpriteNode(imageNamed:"zombie1")
    
    // Movement variables
    var zombieMovingLeft:Bool = false;
    var zombieMovingRight:Bool = true;
    var zombie1: SKSpriteNode = SKSpriteNode(imageNamed:"zombie1")
    
    override func didMove(to view: SKView) {
        // Set the background color of the app
        self.backgroundColor = SKColor.darkGray;
        
        // -----------------------------
        // Add a background to the game
        // -----------------------------
        // Add a background image
         let bg = SKSpriteNode(imageNamed:"background2")
       
        bg.position = CGPoint(x: self.size.width/2, y: self.size.height/2)

        addChild(bg)
        
        
        // -----------------------------
        // Add a zombie to the game
        // -----------------------------
        
        zombie1.position = CGPoint(x: 400, y: 400)
        
        addChild(zombie1)
        
        //Add Grandma
        
        let maa = SKSpriteNode(imageNamed:"enemy")
        maa.position = CGPoint(x: self.size.width - 100, y: self.size.height/2)
        
        addChild(maa)
        
        
    }
   // var movingLeft = false;
   
    override func update(_ currentTime: TimeInterval) {
//        if(movingLeft == false){
//            zombie1.position = CGPoint(x: self.zombie1.position.x + 30, y: 400)
//        }
//        else{
//            zombie1.position = CGPoint(x: self.zombie1.position.x - 30, y: 400)
//            if(self.zombie1.position.x <= 0){
//                movingLeft = false
//            }
//        }
//        if(self.zombie1.position.x >= self.size.width ){
//            movingLeft = true
//
//        }
        zombie1.position.x = zombie1.position.x + (xd * 10)
          zombie1.position.y = zombie1.position.y + (yd * 10)
        print("my current time is\(currentTime)")
        if(timeOfLastUpdate == nil){
            timeOfLastUpdate = currentTime
        }
        //if more than three seconds passed
        
        var timepass = (currentTime - timeOfLastUpdate)
        if(timepass >= 3){
            print("here is the message")
            timeOfLastUpdate = currentTime
        }
        
        
        
        
       
    
    }
    var xd: CGFloat = 0.0;
    var yd: CGFloat = 0.0;
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let loc = touches.first
        
        if(loc == nil){
            return
        }
        let mouPos = loc!.location(in: self)
      //  zombie1.position = CGPoint(x: mouPos.x, y: mouPos.y)
        let a = mouPos.x - self.zombie1.position.x
        let b = mouPos.y - self.zombie1.position.y
        let d = sqrt((a*a) + (b*b))
        xd = a/d;
        yd = b/d
        
    }
    
}
